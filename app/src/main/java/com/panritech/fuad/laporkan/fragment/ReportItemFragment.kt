package com.panritech.fuad.laporkan.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.activity.AddReportActivity
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.adapter.MyReportItemRecyclerViewAdapter
import com.panritech.fuad.laporkan.model.DepartmentItem

import com.panritech.fuad.laporkan.model.ReportItem
import com.panritech.fuad.laporkan.model.UserItem
import com.panritech.fuad.laporkan.presenter.ReportPresenter
import com.panritech.fuad.laporkan.view.ProgressBarView
import com.panritech.fuad.laporkan.view.ReportView
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity


class ReportItemFragment : Fragment(), ReportView, ProgressBarView {

    private var userDepartment = ""
    private var departmentList: HashMap<String, String> = hashMapOf()
    private var reportList: MutableList<ReportItem> = mutableListOf()
    private var report: MutableList<ReportItem> = mutableListOf()
    private var listener: OnListFragmentInteractionListener? = null
    private var status: String = ""
//    private var department: String = ""
    private lateinit var auth: FirebaseAuth
    private lateinit var reportPresenter: ReportPresenter
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var adapter: MyReportItemRecyclerViewAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var statusSpinner: Spinner
    private lateinit var departmentSpinner: Spinner
    private lateinit var addReport: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        auth = FirebaseAuth.getInstance()
        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference
        adapter = MyReportItemRecyclerViewAdapter(report, listener)
        reportPresenter = ReportPresenter(this, this)
//        addReportItem(reportList)
        getDepartment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reportitem, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list_report)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        Log.e("data", reportList.toString())

        progressBar = view.findViewById(R.id.progressBar)
        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        statusSpinner = view.findViewById(R.id.spinnerStatus)
        departmentSpinner = view.findViewById(R.id.spinnerDepartment)
        addReport = view.findViewById(R.id.addReportActionButton)

        statusSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                status = parent?.getItemAtPosition(position).toString().toLowerCase()
                if (position == 0)
                    status = ""
                filterBy(userDepartment,status)
            }
        }

        departmentSpinner.onItemSelectedListener =object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userDepartment = parent?.getItemAtPosition(position).toString()
                if (position == 0)
                    userDepartment = ""
                filterBy(userDepartment,status)
            }
        }

        swipeRefresh.onRefresh {
//            addReportItem(reportList)
//            reportPresenter.getReportList(reportLdataist)
            getDepartment()
        }

        addReport.setOnClickListener {
            startActivity<AddReportActivity>()
        }

        return view
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showReportItem(data: List<ReportItem>) {
        report.clear()
        report.addAll(data)
        filterBy("","")
        adapter.notifyDataSetChanged()
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    private fun filterBy(department: String, status: String){
        adapter.items = reportList.asSequence().filter{
            it.reportDepartment!!.contains(department) && it.reportStatus!!.contains(status)
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    private fun getReportData() {
        val reportEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                reportList.clear()
                for (item in snapshot.children){
                    val data = item.getValue(ReportItem::class.java)

                    if (data != null) {
                        if (data.reporterId == auth.currentUser?.uid.toString())
                            reportList.add(data)

//                        reportList.add(data)
                    }
                }
                reportList.forEach {
                    if (it.reportDepartment != ""){
                        it.reportDepartment = departmentList[it.reportDepartment]
                    }
                }

                reportPresenter.getReportList(reportList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("report").addValueEventListener(reportEventListener)
    }

    private fun getDepartment() {
        val userId = auth.currentUser?.uid
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data: MutableList<DepartmentItem> = mutableListOf()
                snapshot.children.mapNotNullTo(data) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                for (item in data) {
                    departmentList[item.key] = item.name
                }
                Log.e("departmentList", "$data")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        val userDepartmentListener = object :ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val data = snapshot.getValue(UserItem::class.java)
                if (data != null) {
                    userDepartment = data.department
                }
                getReportData()
            }

        }
        myRef.child("department/dpr").addValueEventListener(departmentListListener)
        myRef.child("department/skpd").addValueEventListener(departmentListListener)
        myRef.child("users/$userId").addListenerForSingleValueEvent(userDepartmentListener)
    }


    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: ReportItem)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                ReportItemFragment()
    }
}
