package com.panritech.fuad.laporkan.model

class ReportTable(val reportId: Int, val reportTitle: String, val reportDescription: String, val reportLocation: String
                  , val reportDepartment: String, val reportDate: String, val prosesDate: String, val finishedDate: String
                  , val reportStatus: String, val reporterId: String, val personInChargeId: String)
{
    companion object {
        const val TABLE_REPORT: String = "TABLE_REPORT"
        const val REPORT_ID: String = "REPORT_ID"
        const val REPORT_TITLE: String = "REPORT_TITLE"
        const val REPORT_DESCRIPTION: String = "REPORT_DESCRIPTION"
        const val REPORT_LOCATION: String = "REPORT_LOCATION"
        const val REPORT_DEPARTMENT: String = "REPORT_DEPARTMENT"
        const val REPORT_DATE: String = "REPORT_DATE"
        const val PROSES_DATE: String = "PROSES_DATE"
        const val FINISHED_DATE: String = "FINISHED_DATE"
        const val REPORT_STATUS: String = "REPORT_STATUS"
        const val REPORTER_ID: String = "REPORTER_ID"
        const val PERSON_IN_CHARGE: String = "PERSON_IN_CHARGE"
    }
}
