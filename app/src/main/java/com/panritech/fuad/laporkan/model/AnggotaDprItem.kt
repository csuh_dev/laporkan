package com.panritech.fuad.laporkan.model

data class AnggotaDprItem (
        var dprId: String? = "",
        var dprName: String? = "",
        var dprPosition: String? = "",
        var dprCommission: String? = ""
)