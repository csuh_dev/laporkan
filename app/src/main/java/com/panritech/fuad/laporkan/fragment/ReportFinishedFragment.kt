package com.panritech.fuad.laporkan.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panritech.fuad.laporkan.R
import kotlinx.android.synthetic.main.fragment_report_finished.*

private const val ARG_FINISHED_DATE = "finishedDate"
private const val ARG_FINISHED_DESCRIPTION = "finishedDescription"
private const val ARG_FINISHED_IMG = "finishedImg"

class ReportFinishedFragment : Fragment() {
    private var finishedDate: String? = null
    private var finishedDescription: String? = null
    private var finishedImg: String? = null
    private var listener: OnFragmentInteractionListener? = null


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txt_report_date.text = finishedDate
        txt_report_description.text = finishedDescription
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            finishedDate = it.getString(ARG_FINISHED_DATE)
            finishedDescription = it.getString(ARG_FINISHED_DESCRIPTION)
            finishedImg = it.getString(ARG_FINISHED_IMG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_finished, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(finishedDate: String, finishedDescription: String, finishedImg:String) =
                ReportFinishedFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_FINISHED_DATE, finishedDate)
                        putString(ARG_FINISHED_DESCRIPTION, finishedDescription)
                        putString(ARG_FINISHED_IMG, finishedImg)
                    }
                }
    }
}
