package com.panritech.fuad.laporkan.presenter

import android.util.Log
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.model.ReportItem
import com.panritech.fuad.laporkan.view.ProgressBarView
import com.panritech.fuad.laporkan.view.ReportView

class ReportPresenter (private val reportView: ReportView,
                       private val progressBarView: ProgressBarView){

    fun getReportList(data: MutableList<ReportItem>){
        progressBarView.showProgressBar()
//        val reportListListener = object : ValueEventListener {
//            override fun onDataChange(snapshot: DataSnapshot) {
//                snapshot.children.mapNotNullTo(data) {
//                    it.getValue<ReportItem>(ReportItem::class.java)
//                }
//                Log.e("report list", "${data}")
//            }
//
//            override fun onCancelled(error: DatabaseError) {
//                Log.e("loadPost:Cancelled", "${error.toException()}")
//            }
//        }
//        myRef.child("report").addValueEventListener(reportListListener)
        reportView.showReportItem(data)
    }
}