package com.panritech.fuad.laporkan.view

import com.panritech.fuad.laporkan.model.AnggotaDprItem
import com.panritech.fuad.laporkan.model.ReportItem

interface ProgressBarView {
    fun showProgressBar()
    fun hideProgressBar()
}