package com.panritech.fuad.laporkan.presenter

import android.util.Log
import com.panritech.fuad.laporkan.model.DepartmentItem
import com.panritech.fuad.laporkan.view.DinasView
import com.panritech.fuad.laporkan.view.ProgressBarView

class DinasPresenter (private val dinasView: DinasView, private val progressBarView: ProgressBarView){

    fun getDinasList(data: List<DepartmentItem>){
        progressBarView.showProgressBar()
        dinasView.showDinasItem(data)
    }
}