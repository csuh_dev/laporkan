package com.panritech.fuad.laporkan.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.R.id.imageReport
import com.panritech.fuad.laporkan.R.id.reportCardView
import com.panritech.fuad.laporkan.fragment.ReportItemFragment
import com.panritech.fuad.laporkan.model.ReportItem
import com.panritech.fuad.laporkan.model.UserItem
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import java.text.SimpleDateFormat

class MyReportItemRecyclerViewAdapter(
        var items: MutableList<ReportItem>,
        private val mListener: ReportItemFragment.OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<MyReportItemRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_reportitem_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtDepartment:TextView = view.find(R.id.txtDepartment)
        val txtDate:TextView = view.find(R.id.txtDate)
        val txtReportTitle:TextView = view.find(R.id.txtReportTitle)
        val txtReportDescription:TextView = view.find(R.id.txtReportDescription)
        val txtLocation:TextView = view.find(R.id.txtLocation)
        val txtReportStatus:TextView = view.find(R.id.txtReportStatus)
        val imgReport:ImageView = view.find(R.id.imgReport)

        fun bindItem(items: ReportItem){
            Log.e("reportImg", items.reportImg)
            if(items.reportImg!=""){
                Picasso.get()
                        .load(items.reportImg.toString())
                        .resize(400,300)
                        .centerCrop()
                        .into(imgReport)
            }

            txtDepartment.text = items.reportDepartment
            txtDate.text = items.reportDate
            txtReportTitle.text = items.reportTitle
            txtReportDescription.text = items.reportDescription
            txtLocation.text = items.reportLocation
            txtReportStatus.text = items.reportStatus

            itemView.setOnClickListener {
                mListener?.onListFragmentInteraction(items)
            }
        }

        private fun toDateFormat(date: String): String{
            val dateDb = date
            val formatDb = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ")
            val date = formatDb.parse(dateDb)
            val dateShow = SimpleDateFormat("dd MMM yyyy").format(date)
            return  dateShow.toString()
        }

        private fun getUserName(reporterId: String){
            val userListListener = object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("loadUser:onCancelled", "${error.toException()}")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val data = snapshot.getValue(String::class.java)
                    Log.e("Reporter", data.toString())
                    if (data != null) {
//                        userName =  data.toString()
                    }
                }
            }
//            myRef.child("users/${reporterId}/name").addValueEventListener(userListListener)
        }

        private fun getDepartmentName(reportDepartment: String){
            val userListListener = object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("loadUser:onCancelled", "${error.toException()}")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val data = snapshot.getValue(String::class.java)
                    Log.e("Department", data.toString())
                    if (data != null) {
//                        departmentName = data
                    }
                }
            }
            Log.e("ReporterId", reportDepartment)
            if (reportDepartment.length < 21) {
                //dpr
//                myRef.child("department/dpr/${reportDepartment}/name").addListenerForSingleValueEvent(userListListener)
            } else {
                //skpd
//                myRef.child("department/skpd/${reportDepartment}/name").addListenerForSingleValueEvent(userListListener)
            }

        }
    }
}