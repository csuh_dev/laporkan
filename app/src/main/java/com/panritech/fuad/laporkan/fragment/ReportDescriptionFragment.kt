package com.panritech.fuad.laporkan.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.activity.AddReportActivity
import kotlinx.android.synthetic.main.fragment_report_description.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.yesButton
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso


private const val ARG_REPORT_ID = "reportId"
private const val ARG_REPORT_TITLE = "reportTitle"
private const val ARG_REPORT_DESCRIPTION = "reportDescription"
private const val ARG_REPORT_LOCATION = "reportLocation"
private const val ARG_REPORT_DEPARTMENT = "reportDepartment"
private const val ARG_REPORT_DATE = "reportDate"
private const val ARG_REPORTER_ID = "reporterId"
private const val ARG_REPORT_IMG = "reportImg"

class ReportDescriptionFragment : Fragment() {

    private var reportId: String? = null
    private var reportTitle: String? = null
    private var reportDescription: String? = null
    private var reportLocation: String? = null
    private var reportDepartment: String? = null
    private var reportDate: String? = null
    private var reporterId: String? = null
    private var reportImg: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var fileUri: Uri? = null

    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var mStorageRef: StorageReference

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference
        mStorageRef = FirebaseStorage.getInstance().reference


        txt_report_title.text = reportTitle
        txt_reporter_name.text = reporterId
        txt_report_location.text = reportLocation
        txt_person_in_charge.text = reportDepartment
        txt_report_date.text = reportDate
        txt_report_description.text = reportDescription
        if(reportImg!=""){
            Picasso.get().load(reportImg.toString()).resize(400,300).centerCrop().into(imageView2)
        }

        btnEditReport.onClick {
            startActivity<AddReportActivity>(
                    "reportId" to reportId,
                    "reportTitle" to reportTitle,
                    "reportDescription" to reportDescription,
                    "reportLocation" to reportLocation,
                    "reportImg" to reportImg
            )
        }
        btnDeleteReport.onClick { deleteAlertDialog() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            reportId = it.getString(ARG_REPORT_ID)
            reportTitle = it.getString(ARG_REPORT_TITLE)
            reportDescription = it.getString(ARG_REPORT_DESCRIPTION)
            reportDate = it.getString(ARG_REPORT_DATE)
            reportLocation = it.getString(ARG_REPORT_LOCATION)
            reportDepartment = it.getString(ARG_REPORT_DEPARTMENT)
            reporterId = it.getString(ARG_REPORTER_ID)
            reportImg = it.getString(ARG_REPORT_IMG)
        }
        Log.e("onCreate " , "$reportId - $reportTitle - $reportDescription - $reportDate")

    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_description, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun deleteAlertDialog(){
        alert("Anda Yakin ingin menghapus?", "Konfirmasi Hapus"){
            yesButton {
                // hapus data
                myRef.child("report/$reportId").removeValue()
                listener?.onDeleteClickListener(true)
                toast("OK")
            }
            noButton {  }
        }.show()
    }

    interface OnFragmentInteractionListener {
        fun onDeleteClickListener(isDelete: Boolean)
    }

    companion object {

        @JvmStatic
        fun newInstance(reportId: String, reportTitle: String, reportDescription: String,
                        reportLocation: String, reportDepartment: String,
                        reportDate: String, reporterId: String, reportImg: String) =
                ReportDescriptionFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_REPORT_ID, reportId)
                        putString(ARG_REPORT_TITLE, reportTitle)
                        putString(ARG_REPORT_DESCRIPTION, reportDescription)
                        putString(ARG_REPORT_LOCATION, reportLocation)
                        putString(ARG_REPORT_DEPARTMENT, reportDepartment)
                        putString(ARG_REPORT_DATE, reportDate)
                        putString(ARG_REPORTER_ID, reporterId)
                        putString(ARG_REPORT_IMG, reportImg)
                    }
                Log.e("newInstance : ", "$reportId - $reportTitle - $reportDescription - $reportDate - $reporterId")
                }
    }
}
