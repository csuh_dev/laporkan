package com.panritech.fuad.laporkan.view

import com.panritech.fuad.laporkan.model.DepartmentItem

interface DinasView {
    fun showDinasItem(data: List<DepartmentItem>)
}