package com.panritech.fuad.laporkan.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.adapter.MyDinasItemRecyclerViewAdapter
import com.panritech.fuad.laporkan.model.DepartmentItem
import com.panritech.fuad.laporkan.presenter.DinasPresenter
import com.panritech.fuad.laporkan.view.DinasView
import com.panritech.fuad.laporkan.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_dinas_item.*
import org.jetbrains.anko.support.v4.onRefresh

class DinasItemActivity : AppCompatActivity(), DinasView, ProgressBarView{

    private var dinas: MutableList<DepartmentItem> = mutableListOf()
    private var dinasItem: MutableList<DepartmentItem> = mutableListOf()
    private lateinit var dinasPresenter: DinasPresenter
    private lateinit var adapter: MyDinasItemRecyclerViewAdapter

    private lateinit var auth: FirebaseAuth
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showDinasItem(data: List<DepartmentItem>) {
        dinasItem.clear()
        dinasItem.addAll(data)
        Log.e("data show", data.toString())
        Log.e("dinas show", dinas.toString())
        hideProgressBar()
        adapter.notifyDataSetChanged()
        swipeRefresh.isRefreshing = false
    }

    private fun addDinas(){
        for (i in 0..8) {
            val item = DepartmentItem(i.toString(), "Nama Dinas", "Penanggungjawab")
            dinas.add(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dinas_item)
        supportActionBar?.elevation = 0F
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Dinas"

        auth = FirebaseAuth.getInstance()
        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference

        getInstansiData()

        dinasPresenter = DinasPresenter(this, this)
        list_dinas.layoutManager = LinearLayoutManager(this)
        Log.e("data", dinas.toString())
        adapter = MyDinasItemRecyclerViewAdapter(dinasItem)
        list_dinas.adapter = adapter

        swipeRefresh.onRefresh {
            getInstansiData()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun getInstansiData() {
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data: MutableList<DepartmentItem> = mutableListOf()
                snapshot.children.mapNotNullTo(data) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                Log.e("data", "$data")
                dinas = data
                Log.e("dinas", "$dinas")
                dinasPresenter.getDinasList(dinas)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("department/skpd").addValueEventListener(departmentListListener)
    }
}
