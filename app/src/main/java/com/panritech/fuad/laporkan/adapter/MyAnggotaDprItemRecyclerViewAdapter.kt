package com.panritech.fuad.laporkan.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.activity.AnggotaDprItemActivity
import com.panritech.fuad.laporkan.fragment.ReportItemFragment
import com.panritech.fuad.laporkan.model.AnggotaDprItem
import com.panritech.fuad.laporkan.model.UserItem
import org.jetbrains.anko.find

class MyAnggotaDprItemRecyclerViewAdapter(
        var items: MutableList<UserItem>)
    : RecyclerView.Adapter<MyAnggotaDprItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_anggota_dpr_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtName:TextView = view.find(R.id.txtName)
        val txtCommission:TextView = view.find(R.id.txtCommission)
        val txtPosition:TextView = view.find(R.id.txtPosition)

        fun bindItem(items: UserItem){
            txtName.text = items.name
            txtCommission.text = items.department
            txtPosition.text = items.level
        }
    }
}