package com.panritech.fuad.laporkan.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.model.UserItem
import com.panritech.fuad.laporkan.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference

        auth = FirebaseAuth.getInstance()
        btn_sign_up.onClick {
            validateForm()
        }


    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun register(user: UserItem, password: String){
        auth.createUserWithEmailAndPassword(user.email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in: success
                        // update UI for current User
                        val fireUser = auth.currentUser
                        addUserDetail(user)
                        updateUI(fireUser)
                    } else {
                        // Sign in: fail
                        updateUI(null)
                    }
                }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            snackbar(root_view, "Berhasil Mendaftar")
            startActivity<MainActivity>()
            finish()
        } else {
            snackbar(root_view, "Gagal Mendaftar")
        }
    }

    private fun validateForm() {
        val dialog = progressDialog("Verifikasi Form")
        dialog.show()
        dialog.progress = 50
        val userName = txt_user_name.text
        val userEmail = txt_email.text
        val userPassword = txt_password.text
        val confirmPassword = txt_confirm_password.text
        val phoneNumber = txt_phone_number.text
        val location = txt_alamat.text
        val gender = if (radio_btn_man.isChecked)
            "laki-laki"
        else
            "wanita"

        dialog.progress = 80
        if (userName.isNotEmpty() && userEmail.isNotEmpty() && userPassword.isNotEmpty() && phoneNumber.isNotEmpty() && location.isNotEmpty()) {
            if ("$confirmPassword" == "$userPassword" || userPassword.length >= 6) {
                val user = UserItem("", "$userEmail", "$userName", gender, "$phoneNumber"
                        , "", "$location", "masyarakat", "", "active")
                alertStoreData(user, "$userPassword")
            } else {
                txt_password.backgroundResource = R.drawable.card_stroke_red
                txt_confirm_password.backgroundResource = R.drawable.card_stroke_red
                snackbar(root_view, "Periksa kembali password anda, minimal 6 character")
            }
        } else {
            snackbar(root_view, "Isi Semua Form")
        }
        dialog.progress = 100
        dialog.cancel()
    }

    private fun alertStoreData(user: UserItem, password: String) {
        alert("Yakin data sudah benar?", "Daftarkan akun") {
            yesButton {
                register(user, password)
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    private fun addUserDetail(user: UserItem) {
        val key = auth.currentUser?.uid
        user.uuid = key.toString()
        myRef.child("users/$key").setValue(user).addOnSuccessListener {
            Log.i("firebase:database", "Success Write Data")
        }
    }
}
