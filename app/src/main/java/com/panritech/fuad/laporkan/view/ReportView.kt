package com.panritech.fuad.laporkan.view

import com.panritech.fuad.laporkan.model.ReportItem

interface ReportView {
    fun showReportItem(data: List<ReportItem>)
}