package com.panritech.fuad.laporkan.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panritech.fuad.laporkan.R
import kotlinx.android.synthetic.main.fragment_report_process.*

private const val ARG_PROSES_DATE = "prosesDate"
private const val ARG_PROSES_DESCRIPTION = "prosesDescription"
private const val ARG_PROSES_IMG = "prosesImg"

class ReportProcessFragment : Fragment() {
    private var prosesDate: String? = null
    private var prosesDescription: String? = null
    private var prosesImg: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txt_report_date.text = prosesDate
        txt_report_description.text = prosesDescription
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            prosesDate = it.getString(ARG_PROSES_DATE)
            prosesDescription = it.getString(ARG_PROSES_DESCRIPTION)
            prosesImg = it.getString(ARG_PROSES_IMG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_process, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(prosesDate: String, prosesDescription: String, prosesImg:String) =
                ReportProcessFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PROSES_DATE, prosesDate)
                        putString(ARG_PROSES_DESCRIPTION, prosesDescription)
                        putString(ARG_PROSES_IMG, prosesImg)
                    }
                }
    }
}
