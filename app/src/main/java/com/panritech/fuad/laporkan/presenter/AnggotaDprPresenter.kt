package com.panritech.fuad.laporkan.presenter

import com.panritech.fuad.laporkan.model.AnggotaDprItem
import com.panritech.fuad.laporkan.model.ReportItem
import com.panritech.fuad.laporkan.model.UserItem
import com.panritech.fuad.laporkan.view.AnggotaDprView
import com.panritech.fuad.laporkan.view.ProgressBarView
import com.panritech.fuad.laporkan.view.ReportView

class AnggotaDprPresenter (private val anggotaDprView: AnggotaDprView, private val progressBarView: ProgressBarView){

    fun getAnggotaDprList(data: List<UserItem>){
        progressBarView.showProgressBar()
        anggotaDprView.showAnggotaDprItem(data)
    }
}