package com.panritech.fuad.laporkan.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.activity.EditProfileActivity
import com.panritech.fuad.laporkan.model.UserItem
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.startActivity

private const val ARG_PARAM1 = "param1"

class ProfileFragment : Fragment() {

    private var param1: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private var userItem: UserItem? = UserItem()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference
        auth = FirebaseAuth.getInstance()

        val userUid = auth.currentUser!!.uid
        loadData(userUid)

        txt_btn_edit_profil.onClick {
            Log.e("Value3", userItem.toString())
            startActivity<EditProfileActivity>(
                    "userId" to userUid,
                    "userEmail" to userItem?.email,
                    "userName" to userItem?.name,
                    "userGender" to userItem?.gender,
                    "userPhone" to userItem?.phone,
                    "userDepartment" to userItem?.department,
                    "userLocation" to userItem?.location,
                    "userType" to userItem?.type,
                    "userLevel" to userItem?.level,
                    "userStatus" to userItem?.status
            )
        }

        btn_logout.onClick {
            listener?.onLogoutInteraction(true)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun loadData(data: String) {
        val dialog = indeterminateProgressDialog("Mengambil Data")
        dialog.show()
        val profileListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(UserItem::class.java)
                Log.e("Value", user.toString())
                if (user != null) {
                    userItem = user
                    txt_user_name.text = user.name
                    txt_user_phone_number.text = user.phone
                    txt_user_email.text = user.email
                    txt_user_gender.text = user.gender
                    txt_user_city.text = user.location
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        val reportListListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var reportPending = 0
                var reportProcess = 0
                var reportFinished = 0
                for (item in snapshot.children) {
                    val reportStatus = item.child("reportStatus").getValue(String::class.java)
                    val reportReporterId = item.child("reporterId").getValue(String::class.java)
                    Log.e("Report Status", reportStatus)
                    if (reportReporterId == data) {
                        when (reportStatus) {
                            "new" -> reportPending += 1
                            "process" -> reportProcess += 1
                            "finished" -> reportFinished += 1
                        }
                    }
                }
                txt_total_new_report.text = "$reportPending"
                txt_total_process_report.text = "$reportProcess"
                txt_total_finished_report.text = "$reportFinished"

                dialog.cancel()
            }
        }
        myRef.child("users/$data").addValueEventListener(profileListener)
        myRef.child("report").addValueEventListener(reportListListener)
    }

    interface OnFragmentInteractionListener {
        fun onLogoutInteraction(logout: Boolean)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String) =
                ProfileFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                    }
                }
    }
}
