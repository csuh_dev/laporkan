package com.panritech.fuad.laporkan.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.model.DepartmentItem
import com.panritech.fuad.laporkan.model.DinasItem
import org.jetbrains.anko.find

class MyDinasItemRecyclerViewAdapter(
        var items: MutableList<DepartmentItem>)
    : RecyclerView.Adapter<MyDinasItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_dinas_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtDinasName:TextView = view.find(R.id.txtDinasName)
        val txtDesc:TextView = view.find(R.id.txtDescription)

        fun bindItem(items: DepartmentItem){
            txtDinasName.text = items.name
            txtDesc.text = items.description
        }
    }
}