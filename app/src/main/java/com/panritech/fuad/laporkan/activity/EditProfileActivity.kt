package com.panritech.fuad.laporkan.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.model.UserItem
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.sdk25.coroutines.onClick

class EditProfileActivity : AppCompatActivity() {

    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private lateinit var userItem: UserItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        supportActionBar?.title = "Edit Profil"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference
        auth = FirebaseAuth.getInstance()

        txt_user_name.setText(intent.getStringExtra("userName"))
        txtNumberPhone.setText(intent.getStringExtra("userPhone"))
        txtEmail.setText(intent.getStringExtra("userEmail"))
        txtGender.setText(intent.getStringExtra("userGender"))
//        txtBirthday.setText(intent.getStringExtra("userName"))
        txtAddress.setText(intent.getStringExtra("userLocation"))


        btnSave.onClick {
            userItem = UserItem(
                    intent.getStringExtra("userId"),
                    txtEmail.text.toString(),
                    txt_user_name.text.toString(),
                    txtGender.text.toString(),
                    txtNumberPhone.text.toString(),
                    intent.getStringExtra("userDepartment"),
                    txtAddress.text.toString(),
                    intent.getStringExtra("userType"),
                    intent.getStringExtra("userLevel"),
                    intent.getStringExtra("userStatus")
            )
            Log.e("Value" , userItem.toString())
            updateProfile(userItem)
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun updateProfile(user: UserItem){
        val dialog = indeterminateProgressDialog("Mengambil Data")
        dialog.show()
        myRef.child("users/${user.uuid}/name").setValue(user.name)
        myRef.child("users/${user.uuid}/phone").setValue(user.phone)
        myRef.child("users/${user.uuid}/email").setValue(user.email)
        myRef.child("users/${user.uuid}/gender").setValue(user.gender)
        myRef.child("users/${user.uuid}/location").setValue(user.location)
        dialog.cancel()
    }
}