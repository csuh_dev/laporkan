package com.panritech.fuad.laporkan.model

data class DinasItem (
        var dinasId: String? = "",
        var dinasName: String? = "",
        var dinasDescription: String? = ""
)