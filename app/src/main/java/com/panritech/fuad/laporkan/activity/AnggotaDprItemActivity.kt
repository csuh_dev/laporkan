package com.panritech.fuad.laporkan.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.adapter.MyAnggotaDprItemRecyclerViewAdapter
import com.panritech.fuad.laporkan.model.AnggotaDprItem
import com.panritech.fuad.laporkan.model.DepartmentItem
import com.panritech.fuad.laporkan.model.UserItem
import com.panritech.fuad.laporkan.presenter.AnggotaDprPresenter
import com.panritech.fuad.laporkan.view.AnggotaDprView
import com.panritech.fuad.laporkan.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_anggota_dpr_item.*
import org.jetbrains.anko.support.v4.onRefresh

class AnggotaDprItemActivity : AppCompatActivity(), AnggotaDprView, ProgressBarView {

    private var dpr: MutableList<UserItem> = mutableListOf()
    private var dprItem: MutableList<UserItem> = mutableListOf()
    private var commission: String = ""
    private lateinit var anggotaDprPresenter: AnggotaDprPresenter
    private lateinit var adapter: MyAnggotaDprItemRecyclerViewAdapter

    private lateinit var auth: FirebaseAuth
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showAnggotaDprItem(data: List<UserItem>) {
        dprItem.clear()
        dprItem.addAll(data)
        Log.e("data show", data.toString())
        Log.e("dinas show", dpr.toString())
        hideProgressBar()
        adapter.notifyDataSetChanged()
        swipeRefresh.isRefreshing = false
    }

    private fun addAnggotaDpr(){
        var commission = ""
        var position = ""
        for (i in 0..8) {
            val statusInt = i%3
            when(statusInt){
                0 -> {
                    commission = "Komisi 1"
                    position = "Ketua"
                }
                1 -> {
                    commission = "Komisi 2"
                    position = "Wakil Ketua"
                }
                2 -> {
                    commission = "Komisi 3"
                    position = "Anggota"
                }
            }
            val item = UserItem(i.toString(), "Nama Anggota Dpr", position,
                    commission)
            dprItem.add(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anggota_dpr_item)
        supportActionBar?.elevation = 0F
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Anggota DPR"

        auth = FirebaseAuth.getInstance()
        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference

        getInstansiData()

        anggotaDprPresenter = AnggotaDprPresenter(this, this)
        list_anggota_dpr.layoutManager = LinearLayoutManager(this)
        Log.e("data", dpr.toString())
        adapter = MyAnggotaDprItemRecyclerViewAdapter(dprItem)
        list_anggota_dpr.adapter = adapter


//        anggotaDprPresenter.getAnggotaDprList(anggotaDpr)


        spinnerCommission.onItemSelectedListener =object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                commission = parent?.getItemAtPosition(position).toString()
                if (position == 0)
                    commission = ""
                filterBy(commission)
            }
        }

        swipeRefresh.onRefresh {
            getInstansiData()
//            anggotaDprPresenter.getAnggotaDprList(anggotaDpr)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun filterBy(commission: String){
        adapter.items = dprItem.asSequence().filter{
            it.department.contains(commission)
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    private fun getInstansiData() {
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                dpr.clear()
                for (item in snapshot.children){
                    val data = item.getValue(UserItem::class.java)
                    Log.e("value", data.toString())
                    if (data != null) {
                        if (data.type == "dpr")
                            dpr.add(data)
                    }
                }
                Log.e("dpr", dpr.toString())
                anggotaDprPresenter.getAnggotaDprList(dpr)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("users").addValueEventListener(departmentListListener)
    }
}
