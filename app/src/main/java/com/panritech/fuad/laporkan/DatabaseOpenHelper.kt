package com.panritech.fuad.laporkan

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.panritech.fuad.laporkan.model.ReportTable
import org.jetbrains.anko.db.*

class DatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "ReportApp.db", null, 1) {

    companion object {
        private var instance: DatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseOpenHelper {
            if (instance == null)
                instance = DatabaseOpenHelper(ctx.applicationContext)
            return instance as DatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(ReportTable.TABLE_REPORT, true,
                ReportTable.REPORT_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ReportTable.REPORT_TITLE to TEXT,
                ReportTable.REPORT_DESCRIPTION to TEXT,
                ReportTable.REPORT_LOCATION to TEXT,
                ReportTable.REPORT_DEPARTMENT to TEXT,
                ReportTable.REPORT_DATE to TEXT,
                ReportTable.PROSES_DATE to TEXT,
                ReportTable.FINISHED_DATE to TEXT,
                ReportTable.REPORT_STATUS to TEXT,
                ReportTable.REPORTER_ID to TEXT,
                ReportTable.PERSON_IN_CHARGE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ReportTable.TABLE_REPORT, true)
    }
}

val Context.database: DatabaseOpenHelper
    get() = DatabaseOpenHelper.getInstance(applicationContext)

