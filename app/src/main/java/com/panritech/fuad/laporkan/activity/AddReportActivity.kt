package com.panritech.fuad.laporkan.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ImageView
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.model.ReportItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_report.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.image
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.progressDialog
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*


class AddReportActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 2
    private lateinit var imageReport: ImageView
    private var fileUri: Uri? = null
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private var reportItem: ReportItem? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_report)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference
        auth = FirebaseAuth.getInstance()

        imageReport = findViewById(R.id.imageReport)

        val reportId = intent.getStringExtra("reportId")
        txtReportTitle.setText(intent.getStringExtra("reportTitle"))
        txtReportDescription.setText(intent.getStringExtra("reportDescription"))
        txtLocation.setText(intent.getStringExtra("reportLocation"))

        btnSendReport.onClick {
            if (reportId!=null){
                getReportDetail(
                        reportId,
                        "${txtReportTitle.text}",
                        "${txtReportDescription.text}",
                        "${txtLocation.text}"
                )
                toast("Aduan Diedit")
            } else {
                val result = storeReport(
                        "${txtReportTitle.text}",
                        "${txtReportDescription.text}",
                        "${txtLocation.text}"
                )
                if (result)
                    snackbar(addReportRootLayout, "Aduan Dikirim")
                else
                    snackbar(addReportRootLayout, "Aduan Gagal Dikirim")
            }
            finish()
        }

        textBtnDelete.onClick {
            imageReport.setImageBitmap(null)
        }

        textBtnGallery.onClick {
            takePictureGallery()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("InflateParams")
    private fun cameraAlertDialog(){
        val dialogView = LayoutInflater.from(this).inflate(R.layout.camera_alert_dialog_layout, null)

        val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)

        // set message of alert dialog
        dialogBuilder.setMessage("Ambil Gambar Dengan Posisi HP Mendatar/Tidur")
                .setCancelable(false)
                .setPositiveButton("Ambil Gambar") { _, _ ->
                    dispatchTakePictureIntent()
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        val alert = dialogBuilder.create()
        alert.setTitle("Perhatian")
        alert.show()
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_OK){
            when (requestCode){
                REQUEST_IMAGE_CAPTURE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    imageReport.setImageBitmap(imageBitmap)
                }

                REQUEST_IMAGE_GALLERY -> {
                    fileUri = data?.data
                    val image = MediaStore.Images.Media.getBitmap(this.contentResolver, fileUri)
                    imageReport.setImageBitmap(image)
                }

                else -> {
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    private fun takePictureGallery(){
        val pickImageIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(pickImageIntent, REQUEST_IMAGE_GALLERY)
    }

    private fun storeReport(reportTitle: String, reportDescription: String, reportLocation: String): Boolean {
        val dialog = progressDialog("Mohon Tunggu")
        dialog.show()
        dialog.progress = 10

        val current = Calendar.getInstance().time
        val time = SimpleDateFormat("yyyy-MM-dd HH:mm").format(current)

        val key = myRef.child("report").push().key
        val reportDepartment = ""
        val reportDate = time.toString()//now
        val reportStatus = "new"
        val reporterId = auth.currentUser?.uid
        val reportImg = ""
        val prosesDate = ""
        val prosesDescription = ""
        val prosesImg = ""
        val finishedDate = ""
        val finishedDescription = ""
        val finishedImg = ""
        val createdBy = auth.currentUser?.uid
        val createdAt = time.toString()//now
        val updatedBy = ""
        val updateAt = ""
        val report = ReportItem(
                key,
                reportTitle,
                reportDescription,
                reportLocation,
                reportDepartment,
                reportDate,
                reportStatus,
                reporterId,
                reportImg,
                prosesDate,
                prosesDescription,
                prosesImg,
                finishedDate,
                finishedDescription,
                finishedImg,
                createdBy,
                createdAt,
                updatedBy,
                updateAt
        )
        Log.e("Report", report.toString())
        myRef.child("report").child(key!!).setValue(report)
        uploadImage(key)
        dialog.progress = 100
        dialog.cancel()
        return true
    }


    private fun updateReport(reportId: String, reportTitle: String, reportDescription: String, reportLocation: String){
        myRef.child("report/${reportId}/reportTitle").setValue(reportTitle)
        myRef.child("report/${reportId}/reportDescription").setValue(reportDescription)
        myRef.child("report/${reportId}/reportLocation").setValue(reportLocation)
    }

    private fun uploadImage(key: String) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        if (fileUri != null) {
            val reference = storageRef.child("images/report/$key")
            val uploadTask = reference.putFile(fileUri!!)
            uploadTask
                    .addOnFailureListener {
                        Log.e("Upload Error", it.toString())
                    }
                    .addOnSuccessListener {
                        Log.e("Upload Success", "Berhasil Upload Gambar")
                    }
                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>{ task ->
                        if (!task.isSuccessful){
                            task.exception?.let {
                                throw it
                            }
                        }
                        return@Continuation reference.downloadUrl
                    })
                    .addOnCompleteListener {task ->
                        if (task.isSuccessful) {
                            val downloadUri = task.result
                            Log.e("Download Uri",downloadUri.toString())
                            myRef.child("report/$key/reportImg").setValue(downloadUri.toString())
                        }else{
                            toast("Fail Get Uri")
                        }
                    }
        }
    }



    private fun getReportDetail(reportId: String, reportTitle: String, reportDescription: String, reportLocation: String) {
        val dialog = indeterminateProgressDialog("Mengalihkan Laporan")
        dialog.show()
        val reportDetail = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val item = snapshot.getValue(ReportItem::class.java)
                reportItem = item
                Log.e("item", reportItem.toString())
                reportItem?.reportTitle = reportTitle
                reportItem?.reportDescription = reportDescription
                reportItem?.reportLocation = reportLocation
                myRef.child("report/$reportId").setValue(reportItem)
                uploadImage(reportId)
                Log.e("item2", reportItem.toString())
                Log.e("editData", "Berhasil Edit Data")
                dialog.cancel()
            }
        }
        myRef.child("report/$reportId").addListenerForSingleValueEvent(reportDetail)
    }

}