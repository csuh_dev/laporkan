package com.panritech.fuad.laporkan.model

data class UserItem (
        var uuid: String = "",

        var email: String = "",

        var name: String = "",

        var gender: String = "",

        var phone: String = "",

        var department: String = "",

        var location: String = "",

        var type: String = "",

        var level: String = "",

        var status: String = ""
)