package com.panritech.fuad.laporkan.model

data class DepartmentItem (
        var key: String = "",
        var name: String = "",
        var description: String = ""
)