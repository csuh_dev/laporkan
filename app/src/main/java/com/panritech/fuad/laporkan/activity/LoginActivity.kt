package com.panritech.fuad.laporkan.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.panritech.fuad.laporkan.R
import com.panritech.fuad.laporkan.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity(), ProgressBarView {
    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        hideProgressBar()

        btnLogin.onClick {
            validateForm()
        }

        btnRegister.setOnClickListener {
            startActivity<RegisterActivity>()
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        showProgressBar()
        layout_form.visibility = View.INVISIBLE
        updateUI(currentUser)
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn : $email")
        showProgressBar()

        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = auth.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        toast("Gagal Login")
                        updateUI(null)
                    }
                    // [START_EXCLUDE]
                    if (!task.isSuccessful) {
                        txt_login_status.setText(R.string.txt_user_tidak_ditemukan)
                    }
                    hideProgressBar()
                    // [END_EXCLUDE]
                }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            startActivity<MainActivity>()
            finish()
        } else {
            layout_form.visibility = View.VISIBLE
        }
        hideProgressBar()
    }

    private fun validateForm() {
        val userEmail = txt_user_email.text.toString()
        val userPassword = txt_user_password.text.toString()
        if (userEmail.isNotEmpty() || userPassword.isNotEmpty()) {
            showProgressBar()
            layout_form.visibility = View.INVISIBLE
            signIn(userEmail, userPassword)
        } else {
            txt_login_status.setText(R.string.txt_form_declined)
        }
    }

    companion object {
        private const val TAG = "EmailPassword"
    }
}
