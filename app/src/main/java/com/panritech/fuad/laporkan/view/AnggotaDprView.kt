package com.panritech.fuad.laporkan.view

import com.panritech.fuad.laporkan.model.UserItem

interface AnggotaDprView {
    fun showAnggotaDprItem(data: List<UserItem>)
}