package com.panritech.fuad.laporkan.model

data class ReportItem (
        var reportId: String? = "",
        var reportTitle: String? = "",
        var reportDescription: String? = "",
        var reportLocation: String? = "",
        var reportDepartment: String? = "",
        var reportDate: String? = "",
        var reportStatus: String? = "",
        var reporterId: String? = "",
        var reportImg: String? = "",
        var prosesDate: String? = "",
        var prosesDescription: String? = "",
        var prosesImg: String? = "",
        var finishedDate: String? = "",
        var finishedDescription: String? = "",
        var finishedImg: String? = "",
        var createdBy: String? = "",
        var createdAt: String = "",
        var updatedBy: String ="",
        var updateAt: String = ""
)